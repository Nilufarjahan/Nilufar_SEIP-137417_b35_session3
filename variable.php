<?php
$a=123;
echo "This is integer";
echo "</br>";
echo $a;
echo "</br>";
$b=12.54543;
echo "This is float";
echo "</br>";
echo $b;
echo "</br>";
$c="nilufar";
echo "This is string";
echo "</br>";
echo $c;
echo "</br>";
$d=true;
echo "This is boolean";
echo "</br>";
echo $d;
$e=<<<EOD
this is heredoc $a
heredoc
EOD;
echo "</br>";
echo $e;
echo "</br>";
$f=<<<'EOD'
this is nowdoc $a
nowdoc
EOD;
echo "</br>";
echo $f;
echo "</br>";
